# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 14:32:24 2020

@author: Luis Quiroz
"""

import tweepy
import json
import pandas as pd


consumer_key= "********"
consumer_secret= "********"
access_token= "********"
access_token_secret= "********"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth,
                 wait_on_rate_limit=True,
                 wait_on_rate_limit_notify=True)


Cuentas=['Nike','adidas','Reebok']


for cuenta in Cuentas:    
    timeline = tweepy.Cursor(api.user_timeline, screen_name=cuenta, tweet_mode="extended").items(3600)
    Columns=["Id","Date","Tweet","Retweets","Likes"]
    Datos=pd.DataFrame(columns=Columns)
    for tweet in timeline:
        text_tweet = tweet.full_text
        data = { 'Id':tweet.id,
                'Date': tweet.created_at.date().strftime("%d %m %Y %H %M %S"),
                'Tweet': text_tweet,
                'Retweets': tweet.retweet_count,
                'Likes': tweet.favorite_count
               }
        Datos=Datos.append(data, ignore_index=True)
    Datos[["Retweets","Likes"]]=Datos[["Retweets","Likes"]].astype(int)
    Datos['Date'] = pd.to_datetime(Datos['Date'], format="%d %m %Y %H %M %S")
    print(cuenta)
    Datos.to_csv('./{}.csv'.format(cuenta), sep=';', index=False)

